import IO from "./src/IO.js"

export const init = () => {
	return new IO();
}

export default {
	init
}
